#include "print.h"

using namespace print;
using namespace std;

void print::printDeviceInfo(
	ostream &out, 
	se3_disco_it &it, 
	uint16_t i) {
	
	uint8_t boxWidth = 8;
	out << "\u250F";
	for (uint8_t i = 0; i < boxWidth; i++)
		out << "\u2501";
	out << "\u2513" << endl;
	out << "\u2503 " << setw(boxWidth - 2) << "secube" << " \u2503 " << "#" << i << endl;
	out << "\u2503 " << setw(boxWidth - 2) << "path" << " \u2503 " << it.device_info.path << endl;
	out << "\u2503 " << setw(boxWidth - 2) << "sno" << " \u2503 ";
	printByteArray(out, it.device_info.serialno, SE3_SN_SIZE);
	out << endl;
	out << "\u2517";
	for (uint8_t i = 0; i < boxWidth; i++)
		out << "\u2501";
	out << "\u251B" << endl;
}

void print::printAlgorithms(
	ostream &out, 
	se3_algo *algorithms, 
	uint16_t count) {

	if (count == 0)
		out << "no algorithms found." << endl;
	for (uint16_t i = 0; i < count; i++) {
		out << "algorithm #" << i << " = { " << endl; 
		out << "  name      : " << algorithms[i].name << endl;
		out << "  type      : " << algorithms[i].type << endl;
		out << "  block_size: " << algorithms[i].block_size << endl;
		out << "  key_size  : " << algorithms[i].key_size << endl;
		out << "}" << endl;
	}
}

void print::printKeys(
	ostream &out, 
	se3_key *keys, 
	uint16_t count) {

	if (count == 0)
		out << "no keys found." << endl;
	for (uint16_t i = 0; i < count; i++) {
		cout << "key #" << i << " = { " << endl; 
		out << "        id : " << keys[i].id << endl;
		out << "  validity : " << keys[i].validity << endl;
		out << "  data_size: " << keys[i].data_size << endl;
		out << "  name_size: " << keys[i].name_size << endl;
		out << "  name     : ";
		for (uint16_t j = 0; j < keys[i].name_size; j++)
			out << keys[i].name[j];
		out << endl;
		out << "}" << endl;
	}
}

void print::printByteArray(
	ostream &out, 
	uint8_t *data, 
	uint16_t size) {

	for (uint16_t i = 0; i < size; i++) {
		if (i % 2 == 0 && i > 0)
			out << " ";
		out << setw(2) << setfill('0') << hex << static_cast<int>(data[i]);
	}
	out << setfill(' ') << dec;
}
