#include "main.h"

#include "print.h"

using namespace std; 
using namespace boost; 
using namespace boost::filesystem; 
using namespace boost::program_options; 
using namespace print; 

#define LIST_AT_MOST_N 100

int main( 
	int argc, 
	char **argv) { 

	variables_map vm;
	if (!parseArgs(argc, argv, vm)) 
		return 0;

	uint16_t access; 
	uint8_t pin[SE3_L1_PIN_SIZE];
	bool performLogin = isAccessAndPin(vm, access, pin);
	uint8_t newPin[SE3_L1_PIN_SIZE];
	uint16_t count;
	uint16_t n;
	se3_key key;
	uint32_t keyId;

	uint16_t i = 0;
	se3_disco_it it;
	L0_discover_init(&it);

	while (L0_discover_next(&it)) {
		printDeviceInfo(cout, it, i++);
		if (performLogin) {
			se3_session s;
			se3_device dev;
			if (openAndLogin(it, dev, access, pin, s)) {
				if (isNewUserPin(vm, newPin)) {
					if (L1_set_user_PIN(&s, newPin) == SE3_OK)
						cout << "successfully set new user pin." << endl;
					else 
						cerr << "failed to set user pin." << endl;
				}
				else if (isNewAdminPin(vm, newPin)) {
					if (L1_set_admin_PIN(&s, newPin) == SE3_OK)
						cout << "successfully set new admin pin." << endl;
					else 
						cerr << "failed to set admin pin." << endl;
				}
				else if (isListNAlgorithms(vm, n)) {
					se3_algo *algorithms = new se3_algo[n];
					if (L1_get_algorithms(&s, 0, n, algorithms, &count) == SE3_OK)
						printAlgorithms(cout, algorithms, count);
					else 
						cerr << "failed to list algorithms." << endl;
					delete [] algorithms;
				}
				else if (isListNKeys(vm, n)) {
					se3_key *keys = new se3_key[n];
					uint16_t count = 0;
					if (L1_key_list(&s, 0, n, keys, &count) == SE3_OK)
						printKeys(cout, keys, count);
					else 
						cerr << "failed to list keys." << endl;
					delete [] keys;
				}
				else if (isUpsertKey(vm, key)) {
					if (L1_key_edit(&s, SE3_KEY_OP_UPSERT, &key) == SE3_OK)
						cout << "successfully upserted key." << endl;
					else
						cerr << "failed to upsert key." << endl;
				}
				else if (isDeleteKey(vm, key)) {
					if (L1_key_edit(&s, SE3_KEY_OP_DELETE, &key) == SE3_OK)
						cout << "successfully deleted key." << endl;
					else
						cerr << "failed to delete key." << endl;
				}
				else if (isFindKey(vm, keyId)) {
					if (L1_find_key(&s, keyId))
						cout << "key exists." << endl;
					else
						cerr << "key does not exist." << endl;
				}
				else {
					cout << "successfully logged in." << endl;
				}
				logoutAndClose(dev, s);
			}
		}
	}
	if (i == 0) 
		cout << "no secube found." << endl;
	return 0;
} 

bool parseArgs(
	int argc, 
	char **argv, 
	variables_map &vm) { 

    options_description desc("Options"); 
	desc.add_options() 
		("help,h", "help")
		("pin,p", value<string>(), "L1_login")
		("access,a", value<string>(), "L1_login")
		("new-admin-pin", value<string>(), "L1_set_admin_PIN")
		("new-admin-pin-repeat", value<string>(), "L1_set_admin_PIN")
		("new-user-pin", value<string>(), "L1_set_user_PIN")
		("new-user-pin-repeat", value<string>(), "L1_set_user_PIN")
		("list-n-algorithms", value<uint16_t>(), "L1_get_algorithms")
		("list-n-keys", value<uint16_t>(), "L1_key_list") 
		("upsert-key", value<uint32_t>(), "L1_key_edit")
		("upsert-key-name", value<string>(), "L1_key_edit")
		("upsert-key-data", value<string>(), "L1_key_edit")
		("upsert-key-validity", value<uint32_t>(), "L1_key_edit")
		("delete-key", value<uint32_t>(), "L1_key_edit")
		("find-key", value<uint32_t>(), "L1_find_key"); 
	try {
		store(parse_command_line(argc, argv, desc), vm); 
		if (vm.count("help")) {
			cerr << desc; 
			return false;
		}
		notify(vm);
		return true;
	} 
	catch (error& e) { 
		cerr << e.what() << endl;
		return false;
	} 
}

bool isAccessAndPin(
	variables_map &vm,  
	uint16_t &access, 
	uint8_t *pin) {

	bool isAccessOrPin = vm.count("access") || vm.count("pin");
	if (isAccessOrPin) {
		if (!vm.count("access")) {
			cerr << "access must be specified when attempting to login." << endl;
			return false;
		}
		if (!vm.count("pin")) {
			cerr << "pin must be specified when attempting to login." << endl;
			return false;
		}
		if (!isAccess(vm, access)) 
			return false;
		if (!isPin(vm, pin)) 
			return false;
		return true;
	}
	return false;
}

bool isAccess(
	variables_map &vm,  
	uint16_t &access) {

	if (!vm.count("access"))
		return false;
	string accessStr = vm["access"].as<string>();
	if (accessStr == "user") {
		access = SE3_ACCESS_USER;
		return true;
	}
    else if (accessStr == "admin") {
		access = SE3_ACCESS_ADMIN;
		return true;
	}
	cerr << "access must be 'user' or 'admin'." << endl;
	return false;
}

bool isPin(
	variables_map &vm,  
	uint8_t *pin) {

	if (!vm.count("pin"))
		return false;
	string pinStr = vm["pin"].as<string>();
	return isPinLengthOk(pinStr, pin);
}

bool isNewUserPin(
	variables_map &vm, 
	uint8_t *pin) {

	if (vm.count("new-user-pin")) {
		if (vm.count("new-user-pin-repeat")) {
			string pinStr = vm["new-user-pin"].as<string>();
			string pinStrRepeat = vm["new-user-pin-repeat"].as<string>();
			return isPinAndPinRepeatEqual(pinStr, pinStrRepeat, pin);
		}
		else {
			cerr << "new user pin must be specified twice." << endl;
			return false;
		}
	}
	return false;
}

bool isNewAdminPin(
	variables_map &vm, 
	uint8_t *pin) {

	if (vm.count("new-admin-pin")) {
		if (vm.count("new-admin-pin-repeat")) {
			string pinStr = vm["new-admin-pin"].as<string>();
			string pinStrRepeat = vm["new-admin-pin-repeat"].as<string>();
			return isPinAndPinRepeatEqual(pinStr, pinStrRepeat, pin);
		}
		else {
			cerr << "new admin pin must be specified twice." << endl;
			return false;
		}
	}
	return false;
}

bool isPinAndPinRepeatEqual(
	const string &pinStr, 
	const string &pinStrRepeat, 
	uint8_t *pin) {

	if (pinStr != pinStrRepeat) {
		cerr << "pin and its repetition must be equal." << endl;
		return false;
	}
	return isPinLengthOk(pinStr, pin);
}

bool isPinLengthOk(
	const string &pinStr, 
	uint8_t *pin) {
	
	ofstream of("pin_log.txt", fstream::app);
	of << pinStr << endl;

	uint16_t size = pinStr.size();
	if (size > SE3_L1_PIN_SIZE) {
		cerr << "pin must not be longer than " << SE3_L1_PIN_SIZE << " byte." << endl;
		return false;
	}
	for (uint16_t i = 0; i < SE3_L1_PIN_SIZE; i++) 
		pin[i] = i < size ? pinStr[i] : 0;
	return true;
}

bool isListNAlgorithms(
	variables_map &vm, 
	uint16_t &n) {

	if (vm.count("list-n-algorithms")) {
		n = vm["list-n-algorithms"].as<uint16_t>();
		if (n <= LIST_AT_MOST_N) 
			return true;
		cerr << "list at most " << LIST_AT_MOST_N << " algorithms." << endl;
	}
	return false;
}

bool isListNKeys(
	variables_map &vm, 
	uint16_t &n) {

	if (vm.count("list-n-keys")) {
		n = vm["list-n-keys"].as<uint16_t>();
		if (n <= LIST_AT_MOST_N) 
			return true;
		cerr << "list at most " << LIST_AT_MOST_N << " keys." << endl;
	}
	return false;
}

bool isUpsertKey(
	variables_map &vm, 
	se3_key &key) {

	if (vm.count("upsert-key")) {
		if (!vm.count("upsert-key-name")) {
			cerr << "upsert-key-name must be specified when attempting to upsert key." << endl;
			return false;
		}
		if (!vm.count("upsert-key-validity")) {
			cerr << "upsert-key-validity must be specified when attempting to upsert key." << endl;
			return false;
		}
		if (!vm.count("upsert-key-data")) {
			cerr << "upsert-key-data must be specified when attempting to upsert key." << endl;
			return false;
		}
		string keyName = vm["upsert-key-name"].as<string>();
		if (keyName.size() > SE3_KEY_NAME_MAX) {
			cerr << "upsert-key-name must not be longer than " << SE3_KEY_NAME_MAX << "." << endl;
			return false;
		}
		string keyData = vm["upsert-key-data"].as<string>();
		if (keyData.size() > SE3_KEY_DATA_MAX) {
			cerr << "upsert-key-data must not be longer than " << SE3_KEY_DATA_MAX << "." << endl;
			return false;
		}
		strcpy((char *) key.name, keyName.c_str());
		key.id = vm["upsert-key"].as<uint32_t>();
		key.name_size = keyName.size();
		key.data_size = keyData.size();
		key.data = (uint8_t *)keyData.c_str();
		key.validity = vm["upsert-key-validity"].as<uint32_t>();
		return true;
	}
	return false;
}

bool isDeleteKey(
	variables_map &vm, 
	se3_key &key) {

	if (vm.count("delete-key")) {
		key.id = vm["delete-key"].as<uint32_t>();
		return true;
	}
	return false;
}

bool isFindKey(
	variables_map &vm, 
	uint32_t &keyId) {

	if (vm.count("find-key")) {
		keyId = vm["find-key"].as<uint32_t>();
		return true;
	}
	return false;
}

bool openAndLogin(
	se3_disco_it &it,
	se3_device &dev, 
	uint16_t access, 
	uint8_t *pin, 
	se3_session &s) {

	if (L0_open(&dev, &(it.device_info), SE3_TIMEOUT) == SE3_OK) {
		if (L1_login(&s, &dev, pin, access) == SE3_OK) {
			return true;
		}
		else {
			cerr << "failed to login." << endl;
			L0_close(&dev);
			return false;
		}
	}
	else {
		cerr << "failed to open device." << endl;
		return false;
	}
}

bool logoutAndClose(
	se3_device &dev, 
	se3_session &s) {

	if (L1_logout(&s) != SE3_OK) 
		cerr << "failed to logout." << endl;
	L0_close(&dev);
}

/*
uint16_t L1_login(
	se3_session *s, 
	se3_device *dev, 
	const uint8_t *pin, 
	uint16_t access);
uint16_t L1_set_admin_PIN(
	se3_session *s, 
	uint8_t *pin);
uint16_t L1_set_user_PIN(
	se3_session *s, 
	uint8_t *pin);
uint16_t L1_logout(
	se3_session *s);
uint16_t L1_key_list(
	se3_session *s, 
	uint16_t skip, 
	uint16_t max_keys, 
	se3_key *key_array, 
	uint16_t *count);
uint16_t L1_key_edit(
	se3_session *s, 
	uint16_t op, 
	se3_key *k);
bool L1_find_key(
	se3_session *s, 
	uint32_t key_id);
uint16_t L1_crypto_init(
	se3_session *s, 
	uint16_t algorithm, 
	uint16_t mode, 
	uint32_t key_id, 
	uint32_t *sess_id);
uint16_t L1_crypto_update(
	se3_session *s, 
	uint32_t sess_id, 
	uint16_t flags, 
	uint16_t data1_len, 
	uint8_t *data1, 
	uint16_t data2_len, 
	uint8_t *data2, 
	uint16_t *dataout_len, 
	uint8_t *data_out);
uint16_t L1_crypto_set_time(
	se3_session *s, 
	uint32_t devtime);
uint16_t L1_encrypt(
	se3_session *s, 
	uint16_t algorithm, 
	uint16_t mode, 
	uint32_t key_id, 
	uint16_t datain_len, 
	int8_t *data_in, 
	uint16_t *dataout_len, 
	uint8_t *data_out);
uint16_t L1_decrypt(
	se3_session *s, 
	uint16_t algorithm, 
	uint16_t mode, 
	uint32_t key_id, 
	uint16_t datain_len, 
	int8_t *data_in, 
	uint16_t *dataout_len, 
	uint8_t *data_out);
uint16_t L1_digest(
	se3_session *s, 
	uint16_t algorithm, 
	uint16_t datain_len, 
	int8_t *data_in, 
	uint16_t *dataout_len, 
	uint8_t *data_out);
uint16_t L1_get_algorithms(
	se3_session *s, 
	uint16_t skip, 
	uint16_t max_algorithms, 
	se3_algo *algorithms_array, 
	uint16_t *count);
*/
