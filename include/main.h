#ifndef MAIN_H
#define MAIN_H

#include "common.h"

int main( 
	int argc, 
	char **argv); 
bool parseArgs(
	int argc, 
	char **argv, 
	boost::program_options::variables_map &vm); 
bool isAccessAndPin(
	boost::program_options::variables_map &vm,  
	uint16_t &access, 
	uint8_t *pin);
bool isAccess(
	boost::program_options::variables_map &vm,  
	uint16_t &access);
bool isPin(
	boost::program_options::variables_map &vm,  
	uint8_t *pin);
bool isNewUserPin(
	boost::program_options::variables_map &vm, 
	uint8_t *pin);
bool isNewAdminPin(
	boost::program_options::variables_map &vm, 
	uint8_t *pin);
bool isPinAndPinRepeatEqual(
	const std::string &pinStr, 
	const std::string &pinStrRepeat, 
	uint8_t *pin);
bool isPinLengthOk(
	const std::string &pinStr, 
	uint8_t *pin);
bool isListNAlgorithms(
	boost::program_options::variables_map &vm, 
	uint16_t &n);
bool isListNKeys(
	boost::program_options::variables_map &vm, 
	uint16_t &n);
bool isUpsertKey(
	boost::program_options::variables_map &vm, 
	se3_key &key);
bool isDeleteKey(
	boost::program_options::variables_map &vm, 
	se3_key &key);
bool isFindKey(
	boost::program_options::variables_map &vm, 
	uint32_t &keyId);
bool openAndLogin(
	se3_disco_it &it,
	se3_device &dev, 
	uint16_t access, 
	uint8_t *pin, 
	se3_session &s);
bool logoutAndClose(
	se3_device &dev, 
	se3_session &s);

#endif /* MAIN_H */





















