#ifndef PRINT_H
#define PRINT_H

#include "common.h"

namespace print { 

void printDeviceInfo(
	std::ostream &out, 
	se3_disco_it &it, 
	uint16_t i);
void printAlgorithms(
	std::ostream &out, 
	se3_algo *algorithms, 
	uint16_t count);
void printKeys(
	std::ostream &out, 
	se3_key *keys, 
	uint16_t count);
void printByteArray(
	std::ostream &out, 
	uint8_t *data, 
	uint16_t size);

}

#endif /* PRINT_H */

