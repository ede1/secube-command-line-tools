#ifndef MACROS_H
#define MACROS_H

#define FILE_AND_LINE "(in " __FILE__ " on line " + std::to_string(__LINE__) + ")"
#define ASSERT(x) if (!(x)) throw std::runtime_error("assertion failed: " #x " " FILE_AND_LINE); 
#define ASSERT_UNREACHABLE throw std::runtime_error("unreachable assertion failed " FILE_AND_LINE);
#define ASSERT_EXISTS(filename) if (!boost::filesystem::exists(filename)) throw std::runtime_error("'" + (filename) + "' does not exist. ");
#define ASSERT_DOES_NOT_EXIST(filename) if (boost::filesystem::exists(filename)) throw std::runtime_error("'" + (filename) + "' boost::filesystem::exists already. ");

#define DEL_PTR(x) if ((x) != NULL) { delete (x); (x) = NULL; }

#define NOT_IMPLEMENTED throw std::runtime_error("not implemented");

#define OUT(out, x) (out) << (x) << std::endl;
#define OUT_MAP(out, map) { (out) << "{ " << std::endl; for (auto it = (map).begin(); it != (map).end(); it++) (out) << "  " << it->first << " -> " << it->second << std::endl; (out) << "} " << std::endl; }
#define OUT_VEC(out, vec) { (out) << "[ " << std::endl; for (auto it = (vec).begin(); it != (vec).end(); it++) (out) << "  " << *it << std::endl; (out) << "] " << std::endl; }
#define OUT_SET(out, set) { (out) << "{ " << std::endl; for (auto it = (set).begin(); it != (set).end(); it++) (out) << "  " << *it << std::endl; (out) << "} " << std::endl; }

#define COUT(x) OUT(std::cout, (x))
#define COUT_MAP(map) OUT_MAP(std::cout, (map))
#define COUT_VEC(vec) OUT_VEC(std::cout, (vec))
#define COUT_SET(set) OUT_SET(std::cout, (set))

#define DOUT(x) { std::cout << #x << " = "; OUT(std::cout, (x)) }
#define DOUT_MAP(map) { std::cout << #map << " = "; OUT_MAP(std::cout, (map)) }
#define DOUT_VEC(vec) { std::cout << #vec << " = "; OUT_VEC(std::cout, (vec)) }
#define DOUT_SET(set) { std::cout << #set << " = "; OUT_SET(std::cout, (set)) }

#endif /* MACROS_H */


