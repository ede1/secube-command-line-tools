#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>
#include <sstream>
#include <string>
#include <vector>
#include <exception>

#include <boost/filesystem.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#include "SEfile.h"

#include "macros.h"

#endif /* COMMON_H */


