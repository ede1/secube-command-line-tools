Required: 

1) Cmake 3.1+ 
2) Boost 

How to build this thing: 

1) Depending on your operating system you might have to customize the CMakeList.txt file 
2) mkdir build 
3) cd build 
4) cmake ..
5) make

How to run this thing: 

./m -h 
This will print all of the available command line options and the assciated functions from the SEcube API. 

./m
This will print the serial number and the path for every SEcube device that is available. 

./m --pin <pin> --access <user|admin>
This will attempt a login for every SEcube device that is available. The access level can be user or admin and the pin must be the correct pin for the device and the desired access level. The program will print whether the login in attempt was successful. 

./m --pin <pin> --access <user|admin> --new-admin-pin <newAdminPin> --new-admin-pin-repeat <newAdminPin> 
This will change the admin pin for all connected SEcube devices where the login is successful. Changing the pin will only be successful if the access level is admin. 

./m --pin <pin> --access <user|admin> --new-user-pin <newUserPin> --new-user-pin-repeat <newUserPin> 
This will change the user pin for all connected SEcube devices where the login is successful. Changing the pin will only be successful if the access level is admin. 

./m --pin <pin> --access <user|admin> --list-n-algorithms <num>
This will list the first <num> available algorithms for all connected SEcube devices where the login is successful. The number must be smaller that 100. 

./m --pin <pin> --access <user|admin> --list-n-keys <num>
This will list the first <num> available keys for all connected SEcube devices where the login is successful. The number must be smaller that 100. 

./m --pin <pin> --access <user|admin> --find-key <id>
This will check if a key with the id <id> exists for all connected SEcube devices where the login is successful. 

./m --pin <pin> --access <user|admin> --delete-key <id>
This will delete the key with the id <id> if it exists for all connected SEcube devices where the login is successful. 

./m --pin <pin> --access <user|admin> --upsert-key <id> --upsert-key-name <name> --upsert-key-data <data> --upsert-key-validity <validity>
This will insert or update a key with the id <id> for all connected SEcube devices where the login is successful. The new key is given the name <name>, the value <data> and the validity <validity>. 




