cmake_minimum_required (VERSION 3.1)
set (CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -pedantic -Wextra")
project (secubeCmdtool)
add_definitions(-D_GNU_SOURCE)

set (INCLUDE_DIRS ${INCLUDE_DIRS} "include")
set (INCLUDE_DIRS ${INCLUDE_DIRS} "secube")
set (SOURCE_PATHS ${SOURCE_PATHS} "source/*.cpp")
set (SOURCE_PATHS ${SOURCE_PATHS} "secube/*.c")

# BOOST
find_package(Boost 1.36.0)
if (Boost_FOUND)
	set (INCLUDE_DIRS ${INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
	set (LINK_PATHS ${LINK_PATHS} "${Boost_LIBRARY_DIRS}/libboost_filesystem.a")
	set (LINK_PATHS ${LINK_PATHS} "${Boost_LIBRARY_DIRS}/libboost_system.a")
	set (LINK_PATHS ${LINK_PATHS} "${Boost_LIBRARY_DIRS}/libboost_serialization.a")
	set (LINK_PATHS ${LINK_PATHS} "${Boost_LIBRARY_DIRS}/libboost_program_options.a")
endif()

foreach (P ${SOURCE_PATHS})
	file (GLOB_RECURSE TMP "${P}")
	set (SOURCE_FILES ${SOURCE_FILES} ${TMP})
endforeach (P)

foreach (P ${LINK_PATHS})
	file (GLOB_RECURSE TMP "${P}")
	set (LINK_FILES ${LINK_FILES} ${TMP})
endforeach (P)

foreach (P ${RESOURCE_PATHS})
	file (GLOB_RECURSE TMP "${P}")
	set (RESOURCE_FILES ${RESOURCE_FILES} ${TMP})
endforeach (P)

message ("   ")
message (" 1 INCLUDE_DIRS")
foreach (E ${INCLUDE_DIRS})
	message (" + ${E}")
endforeach (E)

message ("   ")
message (" 2 SOURCE_PATHS")
foreach (E ${SOURCE_PATHS})
	message (" + ${E}")
endforeach (E)

message ("   ")
message (" 3 SOURCE_FILES")
foreach (E ${SOURCE_FILES})
	message (" + ${E}")
endforeach (E)

message ("   ")
message (" 4 LINK_PATHS")
foreach (E ${LINK_PATHS})
	message (" + ${E}")
endforeach (E)

message ("   ")
message (" 5 LINK_FILES")
foreach (E ${LINK_FILES})
	message (" + ${E}")
endforeach (E)

message ("   ")
message (" 6 RESOURCE_PATHS")
foreach (E ${RESOURCE_PATHS})
	message (" + ${E}")
endforeach (E)

message ("   ")
message (" 7 RESOURCE_FILES")
foreach (E ${RESOURCE_FILES})
	message (" + ${E}")
endforeach (E)

message ("   ")

include_directories (${INCLUDE_DIRS})
add_executable (m ${SOURCE_FILES})
target_link_libraries (m ${LINK_FILES})
foreach (E ${RESOURCE_FILES})
	file (RELATIVE_PATH E_REL ${PROJECT_SOURCE_DIR} ${E})
	get_filename_component(E_REL_DIR ${E_REL} DIRECTORY)
	file (COPY ${E} DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/${E_REL_DIR}")
endforeach (E)




